<?php

/**
 * Define the affiliate referral admin settings form
 */
function uc_affiliate_referral_admin_settings() {
  $form['affiliate_referral_is_active'] = array(
  	'#type' => 'checkbox',
  	'#title' => t('Affiliate / Referral Module is Active'),
  	'#description' => t('By default, the module will be disabled until someone with priviledges activates the account, and makes changes to this page. 
  		<br /><b>Warning</b>: if you activate this module, import affiliate referrals, and then deactivate this checkbox, then all the affiliate referrals 
  		in the database will be	removed as you are telling the system to turn off this modules functionallity.'),
  	'#default_value' => variable_get('affiliate_referral_is_active', 0),
  );
  $form['affiliate_referral_group'] = array(
  	'#type' => 'fieldset',
  	'#title' => t('Referral Group'),
  );
  $form['affiliate_referral_group']['affiliate_referral_group_is_active'] = array(
  	'#type' => 'checkbox',
  	'#title' => t('Add Affiliates into a Referral Group'),
  	'#description' => t('Allows you to toggle whether or not imported referral affiliates will be part of a Referral group'),
  	'#default_value' => variable_get('affiliate_referral_group_is_active', 1),
  );
  $form['affiliate_referral_group']['affiliate_referral_group_name'] = array(
  	'#type' => 'textfield',
    '#title' => t('The referral group name that affiliates will be added into'),
    '#description' => t('The referral module contains an option to place referrals inside of a group. This will be the group that affiliates will be placed in by default.'),
    '#default_value' => variable_get('affiliate_referral_group_name', 'Affiliates'),
  );
  if (variable_get('affiliate_referral_is_active', 0)) {
		$form['import_affiliates'] = array(
			'#type' => 'submit',
			'#value' => t('Import Affiliates as Referrals'),
			'#weight' => 10,
		);
  }
  $form['#submit'][] = 'uc_affiliate_referral_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * A custom hook_form_submit method to add to the system_settings form so that I can run some custom methods, like importing
 * affiliates from the system_settings form, as well as remove affiliates from the database if the module is deactivated
 */
function uc_affiliate_referral_admin_settings_submit($form, &$form_state) {
	if ($form_state['values']['affiliate_referral_is_active'] == 0) {
		//if the uc_affiliate_referral module is turned off, then remove all database rows associated with the module
		_uc_affiliate_referral_delete_affiliates();
	}
	if ($form_state['clicked_button']['#value'] == 'Import Affiliates as Referrals') {
		_uc_affiliate_referral_select_affiliates_to_import();
	}
}

